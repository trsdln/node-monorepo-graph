#!/usr/bin/env node

import fs from 'fs'
import path from 'path'
import util from 'util'
import { flattenDependencyGraph } from './flatten.js'

const readdirP = util.promisify(fs.readdir)
const readFileP = util.promisify(fs.readFile)

const getPkgDepsByJsonAndProp = (pkgJson, prop) =>
  Object.keys(pkgJson[prop] || {})

const getPkgDepsByJson = (pkgJson) => [
  ...getPkgDepsByJsonAndProp(pkgJson, 'dependencies'),
  ...getPkgDepsByJsonAndProp(pkgJson, 'devDependencies'),
]

const getMonorepoPackages = (dirsWithDeps) => {
  const packagesWithDeps = dirsWithDeps.filter((pkg) => pkg.isPackage === true)
  const monorepoPackageNames = packagesWithDeps.map((pkg) => pkg.name)
  return packagesWithDeps.map((pkg) => ({
    ...pkg,
    deps: pkg.deps.filter((depName) => monorepoPackageNames.includes(depName)),
  }))
}

const idsMap = {}

const getPkgIdByName = (name) => {
  if (!idsMap[name]) {
    const pkgNum = Object.keys(idsMap).length + 1
    idsMap[name] = `P${pkgNum}`
  }

  return idsMap[name]
}

const getPkgMermaidNode = (pkgName, isApp) =>
  `${getPkgIdByName(pkgName)}${isApp ? '(' : ''}[${pkgName}]${isApp ? ')' : ''}`

const pkgToMermaidRows = (pkg) =>
  pkg.deps
    .map(
      (depName) =>
        `  ${getPkgMermaidNode(pkg.name, pkg.isApp)} --> ${getPkgMermaidNode(
          depName
        )}`
    )
    .join('\n')

const depsGraphToMermaid = (packages) => `%% Generated with node-monorepo-graph
flowchart TD
${packages
  .map(pkgToMermaidRows)
  .filter((s) => s !== '')
  .join('\n\n')}
`

const rejectBlacklistedPkgs = (blacklist) => (packages) =>
  packages.filter((pkg) => !blacklist.includes(pkg.name))

const markMonorepoApps = (packages) =>
  packages.map((pkg) => ({
    ...pkg,
    isApp: packages.every((otherPkg) => !otherPkg.deps.includes(pkg.name)),
  }))

const compareStringWith = (getStrFn) => (a, b) => {
  const aVal = getStrFn(a)
  const bVal = getStrFn(b)

  if (aVal > bVal) {
    return 1
  }

  if (aVal < bVal) {
    return -1
  }

  return 0
}

const sortPackages = (packages) =>
  [...packages].sort(compareStringWith((pkg) => pkg.name)).map((pkg) => ({
    ...pkg,
    deps: [...pkg.deps].sort(compareStringWith((dep) => dep)),
  }))

async function main(packagesDir, blacklistPackages) {
  const potentialPackagesDirectories = await readdirP(packagesDir)

  const dirsWithDeps = await Promise.all(
    potentialPackagesDirectories.map(async (pkgName) => {
      let isPackage = true
      let pkgJson = {}

      try {
        const pkgJsonPath = path.join(packagesDir, pkgName, 'package.json')
        pkgJson = JSON.parse(await readFileP(pkgJsonPath, 'utf-8'))
      } catch (err) {
        if (err.code === 'ENOENT') {
          isPackage = false
        } else {
          throw err
        }
      }

      return {
        name: pkgJson.name,
        deps: getPkgDepsByJson(pkgJson),
        isPackage,
      }
    })
  )

  return [
    rejectBlacklistedPkgs(blacklistPackages),
    getMonorepoPackages,
    flattenDependencyGraph,
    markMonorepoApps,
    sortPackages,
    depsGraphToMermaid,
  ].reduce((acc, pipelineStep) => pipelineStep(acc), dirsWithDeps)
}

const blacklistPackages = (process.env.MONOREPO_GRAPH_BLACKLIST || '').split(
  ' '
)

const packagesDir = process.argv[2] || './packages'

/* eslint-disable no-console */
main(packagesDir, blacklistPackages).catch(console.error).then(console.log)
/* eslint-enable no-console */
