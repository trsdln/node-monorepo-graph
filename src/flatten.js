const unnestArray = (arr) => arr.reduce((acc, el) => acc.concat(el), [])

const findPkgByName = (depGraph, pkgName) =>
  depGraph.find((pkg) => pkg.name === pkgName)

function collectPkgDepsRecursively(depGraph, pkgName, currentPath = []) {
  if (currentPath.length > 1 && pkgName === currentPath[0]) {
    const depCyclePath = [...currentPath, pkgName].join(' -> ')
    throw Error(`Dependency cycle: ${depCyclePath}`)
  }
  const currPkg = findPkgByName(depGraph, pkgName)
  const otherDeps = currPkg.deps.map((depName) =>
    collectPkgDepsRecursively(depGraph, depName, [...currentPath, pkgName])
  )
  return [...currPkg.deps, ...unnestArray(otherDeps)]
}

const rejectDuplicatedDeps = (depGraph) => (depsList, targetDepName) => {
  const currPkgAllDeps = collectPkgDepsRecursively(depGraph, targetDepName)
  return depsList.filter((depName) => !currPkgAllDeps.includes(depName))
}

const rejectDuplicatedDepsForPkg = (depGraph) => (targetPkg) => ({
  ...targetPkg,
  deps: targetPkg.deps.reduce(rejectDuplicatedDeps(depGraph), targetPkg.deps),
})

export const flattenDependencyGraph = (depGraph) =>
  depGraph.map(rejectDuplicatedDepsForPkg(depGraph))
