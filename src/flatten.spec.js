import { flattenDependencyGraph } from './flatten.js'

describe('#flattenDependencyGraph', () => {
  it('should remove duplicate dep on 2nd level', () => {
    expect(
      flattenDependencyGraph([
        { name: 'a', deps: ['b', 'c'] },
        { name: 'b', deps: ['c'] },
        { name: 'c', deps: [] },
      ])
    ).toEqual([
      { name: 'a', deps: ['b'] },
      { name: 'b', deps: ['c'] },
      { name: 'c', deps: [] },
    ])
  })

  it('should remove duplicate dep on 3rd level', () => {
    expect(
      flattenDependencyGraph([
        { name: 'a', deps: ['b', 'c'] },
        { name: 'b', deps: ['b2'] },
        { name: 'b2', deps: ['c'] },
        { name: 'c', deps: [] },
      ])
    ).toEqual([
      { name: 'a', deps: ['b'] },
      { name: 'b', deps: ['b2'] },
      { name: 'b2', deps: ['c'] },
      { name: 'c', deps: [] },
    ])
  })

  it('should remove all deps (3rd+2nd)', () => {
    expect(
      flattenDependencyGraph([
        { name: 'b', deps: ['b2', 'c'] },
        { name: 'a', deps: ['b', 'c'] },
        { name: 'b2', deps: ['c'] },
        { name: 'c', deps: [] },
      ])
    ).toEqual([
      { name: 'b', deps: ['b2'] },
      { name: 'a', deps: ['b'] },
      { name: 'b2', deps: ['c'] },
      { name: 'c', deps: [] },
    ])
  })

  it('should throw if cycle dep detected', () => {
    expect(() =>
      flattenDependencyGraph([
        { name: 'a', deps: ['b'] },
        { name: 'b', deps: ['c'] },
        { name: 'c', deps: ['d', 'a'] },
        { name: 'd', deps: [] },
      ])
    ).toThrow('Dependency cycle: b -> c -> a -> b')
  })
})
